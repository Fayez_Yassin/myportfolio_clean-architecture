﻿
using System.Linq;
using Core.Entitys;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Web.ViewModels;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork<Owner> _owner;
        private readonly IUnitOfWork<PortFoliItem> _portfolio;

        public HomeController(IUnitOfWork<Owner> owner, IUnitOfWork<PortFoliItem>portfolio)
        {
            _owner = owner;
            _portfolio = portfolio;
        }
     
        public IActionResult Index()
        {
            var homeviewm = new HomeViewModel()
            { 
            owner=_owner.Entity.GetAll().First(),
            PortFoliItems=_portfolio.Entity.GetAll().ToList()
            };

            //var homeViewModel = new HomeViewModel
        

            return View(homeviewm);
        }

       

    }
}