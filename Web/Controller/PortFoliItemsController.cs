﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Core.Entitys;
using Infrastructure;
using Web.ViewModels;
using System.IO;
using Core.Interfaces;
using Microsoft.AspNetCore.Hosting;

namespace Web.Controllers
{
    public class PortFoliItemsController : Controller
    {
        private readonly IUnitOfWork<PortFoliItem> _portfolio;
        private readonly IHostingEnvironment _hosting;

        public PortFoliItemsController(IUnitOfWork<PortFoliItem> portfolio, IHostingEnvironment hosting)
        {
            _portfolio = portfolio;
            _hosting = hosting;
        }

        // GET: PortFoliItems
        public IActionResult Index()
        {
            return View(_portfolio.Entity.GetAll().ToList());
        }

        // GET: PortFoliItems/Details/5
        public IActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portFoliItem = _portfolio.Entity.GetById(id);

            if (portFoliItem == null)
            {
                return NotFound();
            }

            return View(portFoliItem);
        }

        // GET: PortFoliItems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PortFoliItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PortfolioViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.File != null)
                {
                    String upload = Path.Combine(_hosting.WebRootPath, @"img\portfolio");

                    string fullPath = Path.Combine(upload, model.File.FileName);
                    model.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                }
                PortFoliItem portfolioItem = new PortFoliItem
                {
                    ProjectName = model.ProjectName,
                    Description = model.Description,
                    ImageUrl = model.File.FileName
                };
                _portfolio.Entity.Insert(portfolioItem);
                _portfolio.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: PortFoliItems/Edit/5
        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portFoliItem = _portfolio.Entity.GetById(id);
            if (portFoliItem == null)
            {
                return NotFound();
            }
            PortfolioViewModel portfolioViewModel = new PortfolioViewModel
            {
                Id = portFoliItem.Id,
                Description = portFoliItem.Description,
                ImageUrl = portFoliItem.ImageUrl,
                ProjectName = portFoliItem.ProjectName
            };
            return View(portfolioViewModel);
        }

        // POST: PortFoliItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid id, PortfolioViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (model.File != null)
                    {
                        string uploads = Path.Combine(_hosting.WebRootPath, @"img\portfolio");
                        string fullPath = Path.Combine(uploads, model.File.FileName);
                        model.File.CopyTo(new FileStream(fullPath, FileMode.Create));
                    }

                    PortFoliItem portfolioItem = new PortFoliItem
                    {
                        Id = model.Id,
                        ProjectName = model.ProjectName,
                        Description = model.Description,
                        ImageUrl = model.File.FileName
                    };

                    _portfolio.Entity.Update(portfolioItem);
                    _portfolio.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PortFoliItemExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: PortFoliItems/Delete/5
        public IActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portFoliItem = _portfolio.Entity.GetById(id);
                
            if (portFoliItem == null)
            {
                return NotFound();
            }

            return View(portFoliItem);
        }

        // POST: PortFoliItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            // var portFoliItem = _portfolio.Entity.GetById(id);
            _portfolio.Entity.Delete(id);
            _portfolio.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool PortFoliItemExists(Guid id)
        {
            return _portfolio.Entity.GetAll().Any(e => e.Id == id);
        }
    }
}
