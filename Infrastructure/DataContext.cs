﻿using Core.Entitys;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
  public  class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> option):base(option)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Owner>().Property(x => x.Id).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<PortFoliItem>().Property(x => x.Id).HasDefaultValueSql("NEWID()");

            modelBuilder.Entity<Owner>().HasData(
                new Owner() { 
                Id=Guid.NewGuid(),
                FullName="FAYEZ YASSIN",
                Avatar="fayez.jpg",
                Profil="Asp.net"
                }
                );
        }

        public DbSet<Owner> Owner { get; set; }
        public DbSet<PortFoliItem> PortFoliItems { get; set; }


    }
}
